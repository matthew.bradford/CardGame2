﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autofac;

using CardLib;
using CardLib.Interface;

namespace CardGame
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            Container = Startup.RegisterContainer();
            IList<IHand> playerHands = new List<IHand>();
            var playerCount = StaticRandom.Rand(3, 6);
            var deck = GetDeck();
            deck.Shuffle();

            for (int i =0; i< playerCount; i++)
            {
                playerHands.Add(GetHand());                
            }

            GameLoop loop = new GameLoop(playerHands, deck);

            while (loop.Loop()) ;

            Console.ReadLine();
        }

        private static IDeck GetDeck()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var deal = scope.Resolve<IDeck>();
                return deal;
            }
        }

        private static IHand GetHand()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                return scope.Resolve<IHand>();
            }
        }
    }   
}

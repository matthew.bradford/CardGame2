﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CardLib;
using CardLib.Exceptions;
using CardLib.Interface;
using CardLib.Object;

namespace CardLibTest
{
    [TestClass]
    public class DeckTests
    {
        [TestMethod]
        public void GetCard()
        {
            var card = new Card((CardName)1, Suit.Diamonds);
            var name = card.DispalyName(DeckType.PlayingCards);
            Assert.IsTrue(name.Equals("Ace of Diamonds"));
        }

        [TestMethod]
        public void GetCards()
        {
            var deck = new Decks(1, DeckType.PlayingCards);
            deck.Shuffle();
            var cards = deck.Deal(5);
            Assert.IsTrue(cards.Count == 5);
        }

        [TestMethod]
        [ExpectedException(typeof(DeckOutOfBounds))]
        public void TestDealMoreThanDeck()
        {
            var deck = new Decks(1, DeckType.PlayingCards);
            deck.Shuffle();
            deck.Deal(55);
        }

        [TestMethod]
        [ExpectedException(typeof(DeckOutofCards))]
        public void TestOutofCards()
        {
            var deck = new Decks(1, DeckType.PlayingCards);
            deck.Shuffle();
            deck.Deal(52);
            deck.Deal(1); 
        }

        [TestMethod]
        public void AmIPlayingWithAFullDeck()
        {
            var deck = new Decks(1, DeckType.PlayingCards);
            deck.Shuffle();
            var cards = deck.Deal(deck.Count());
            Assert.IsTrue(cards.Count() == 52);
        }

        [TestMethod]
        public void TwoFullDecks()
        {
            var deck = new Decks(2, DeckType.PlayingCards);
            deck.Shuffle();
            var cards = deck.Deal(deck.Count());
            Assert.IsTrue(cards.Count == 104);
        }

        [TestMethod]
        public void TestDeal()
        {
            //Deal should not leave any of the cards delt in the deck...
            var deck = new Decks(1, DeckType.PlayingCards);
            deck.Shuffle();

            var hand1 = deck.Deal(5);
            var hand2 = deck.Deal(10);
            var hand3 = deck.Deal(20);
            var hand4 = deck.Deal(5);
            var hand5 = deck.Deal(10);
            var hand6 = deck.Deal(2);

            Assert.IsTrue(deck.Count() == 0);
        }

        [TestMethod]
        public void AmIPlayingWithACompleteDeck()
        {
            var deck = new Decks(1, DeckType.PlayingCards);
            deck.Shuffle();
            var cards = deck.Deal(52);
            bool complete = false;
            var checkDeck = TestConstants.GetAFullPlayingCardDeck();

            //using a value based equality check here because the objects being compared
            //are not created the way the cards for games from the library will be consumed
            //most comparisions in the game will be made based on reference equality. Each card
            //will be present in a deck, the stack (shuffled decks), players hands, discard piles,
            //or any othe group of cards a game may need to define. Cards cannot be destroyed,
            //but may not be in the stack becase they are in a hand or the discard.
            complete = cards.Where(
                    x => checkDeck.Any(
                                        y => y.CardSuit == x.CardSuit &&
                                        x.Name == y.Name &&
                                        x.DispalyName(DeckType.PlayingCards) == y.DispalyName(DeckType.PlayingCards)
                                      )
                                    ).Count() == 52;

            Assert.IsTrue(complete);

            //reverse check that all cards from the const deck have a match in gen deck
            complete = checkDeck.Where(
                    x => cards.Any(
                                        y => y.CardSuit == x.CardSuit &&
                                        x.Name == y.Name &&
                                        x.DispalyName(DeckType.PlayingCards) == y.DispalyName(DeckType.PlayingCards)
                                      )
                                    ).Count() == 52;

            Assert.IsTrue(complete);
        }

        [TestMethod]
        public void TestRandomSortIsSortaRandom()
        {
            //This test may fail randomly as 2 random hands of 5 cards could match (In the future this will be resolved as the same card
            //would not be able to be delt twice, but that feature is not yet implemented).
            int testLength = 5;
            var deck = new Decks(1, DeckType.PlayingCards);
            deck.Shuffle();
            var hand1 = deck.Deal(testLength);
            deck.Shuffle();
            var hand2 = deck.Deal(testLength);

            //Without overriding .Equals this should be a refernce comparision. Since each deck is just references
            //to the same objects in the deck array reference comparision is intentional.
            var handsMatch = hand1.SequenceEqual(hand2);

            Assert.IsFalse(handsMatch);
        }

        [TestMethod]
        public void TestSort()
        {
            var deck = new Decks(1, DeckType.PlayingCards);
            var orderedList = deck.OrderCards();
            var ascOrderDeck = TestConstants.GetAFullPlayingCardDeck();

            bool orderAsc = true;
            //Value comparision not reference comparision.
            for (int i = 0; i < orderedList.Count(); i++)
            {
                if (!orderAsc) { break; }
                if (orderedList[i].Name != ascOrderDeck[i].Name || orderedList[i].CardSuit != ascOrderDeck[i].CardSuit)
                {
                    Console.WriteLine($"{orderedList[i].DispalyName()} not equal to {ascOrderDeck[i].DispalyName()}");
                    orderAsc = false;
                }
            }

            //A truer test was never wrote
            Assert.IsTrue(orderAsc);
        }

        //[TestMethod]
        public void TestReturningCards()
        {
            //TODO: Think this out. A Deck contains 52 cards. The Array will always contain all 52 cards in order.
            //On top of that is a List<Deck> object containing all cards available. This object has interface methods
            //to radomly retrieve a set number of cards from the underlying Decks. A card is never removed/destroyed from the
            //base Deck. 
            //Can it be removed from the List<Deck> object?

            Assert.IsTrue(false);
        }

        //[TestMethod]
        public void TestReturningCard()
        {
            //TODO: See TestReturningCards()

            Assert.IsTrue(false);
        }

    }
}

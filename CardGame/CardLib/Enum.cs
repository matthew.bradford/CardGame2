﻿using System;
using System.Collections.Generic;

namespace CardLib
{
    public enum Suit
    {        
        Spades = 1,
        Diamonds = 2,
        Clubs = 3,
        Hearts = 4,
    }

    public enum CardName
    {
        Ace = 1,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,
    }

    public enum DeckType
    {
        PlayingCards,
        Pinochle,
    }

    public static class EnumExt
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return (T[])Enum.GetValues(typeof(T));
        }
    }
}

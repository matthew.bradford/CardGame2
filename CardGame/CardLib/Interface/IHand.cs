﻿using System.Collections.Generic;

namespace CardLib.Interface
{
    public interface IHand
    {
        /// <summary>
        /// Count of cards currently in hand.
        /// </summary>
        int Count { get; }
        /// <summary>
        /// Add cards delt from deck to players hand
        /// </summary>
        /// <param name="cards"></param>
        void Add(IList<ICard> cards);
        /// <summary>
        /// Add card to players hand
        /// </summary>
        /// <param name="card"></param>
        void Add(ICard card);
        /// <summary>
        /// Get all cards in players hand that have the same CardName.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IList<ICard> Get(CardName name);
        /// <summary>
        /// Get a random card name contained in hand.
        /// </summary>
        /// <returns>Cardname</returns>
        CardName RandomCardName();
        /// <summary>
        /// Show the players hand.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ICard> ShowHand();
    }
}

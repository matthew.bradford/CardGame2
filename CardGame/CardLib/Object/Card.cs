﻿using CardLib.Interface;

namespace CardLib.Object
{
    public class Card : ICard
    {
        #region MemberVariables
        readonly CardName _name;
        readonly Suit _cardSuit;
        #endregion MemberVariables

        #region Constructors
        public Card(CardName name, Suit suit)
        {
            _name = name;
            _cardSuit = suit;
        }
        #endregion Constructors

        #region Properties
        //Possibly replace this with an Id instead later. Guids or something allowing for group card games over a rest interface.
        public CardName Name { get { return _name; } }

        public Suit CardSuit { get { return _cardSuit; } }
        #endregion Properties

        #region PublicMethods
        //Get the name of the card that holds a given position in a deck type        
        public string DispalyName(DeckType type)
        {
            string name = string.Empty;

            switch (type)
            {
                case DeckType.PlayingCards:
                    name = PlayingCardName();
                    break;
                case DeckType.Pinochle:
                    name = PinochleCardName();
                    break;
            }

            return name;
        }
        #endregion PublicMethods

        #region PrivateMethods
        private string PlayingCardName()
        {
            return $"{Name} of {CardSuit}";
        }

        private string PinochleCardName()
        {
            var name = string.Empty;

            return name;
        }
        #endregion //PrivateMethods
    }
}
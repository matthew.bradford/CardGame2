﻿using System.Collections.Generic;
using System.Linq;

using CardLib.Exceptions;
using CardLib.Interface;

namespace CardLib.Object
{
    public class Deck
    {
        #region MemberVariables
        private Card[] _cards;
        #endregion MemberVariables

        #region Constructors
        public Deck(DeckType type)
        {
            this.DeckType = type;
            Init(type);
        }
        #endregion Constructors

        #region Properties
        public DeckType DeckType { get; private set; }
        #endregion Properties

        #region PublicMethods
        public IList<ICard> GetCards()
        {
            return _cards.ToList<ICard>();
        }

        public int Count()
        {
            return _cards.Count(x => x != null);
        }
        #endregion PublicMethods

        #region PrivateMethods
        private void Init(DeckType type)
        {
            switch (type)
            {
                case DeckType.PlayingCards:
                    _cards = new Card[52];
                    break;
                case DeckType.Pinochle:
                    _cards = new Card[48];
                    break;
            }

            foreach (Suit suit in EnumExt.GetValues<Suit>())
            {
                SetCardsForSuit(suit);
            }
        }

        private void SetCardsForSuit(Suit suit)
        {
            switch (this.DeckType)
            {
                case DeckType.PlayingCards:
                    CreatePlayingDeckSuit(suit);
                    break;
            }
        }

        private void CreatePlayingDeckSuit(Suit suit)
        {
            int i = 1;
            var suitVal = (int)suit;
            foreach (CardName name in EnumExt.GetValues<CardName>())
            {
                _cards[((suitVal - 1) * 13) + (suitVal + i) - (suitVal + 1)] = new Card(name, suit);
                i++;
            }
        }
        #endregion PrivateMethods
    }

    public class Decks : IDeck
    {
        #region MemberVariables
        IList<Deck> _decks = new List<Deck>();
        List<ICard> _stack = new List<ICard>();
        #endregion MemberVariables

        #region Constructors
        public Decks(int count = 1, DeckType decktype = DeckType.PlayingCards)
        {
            for (int i = 0; i < count; i++)
            {
                _decks.Add(new Deck(decktype));
            }
        }
        #endregion Constructors

        #region PublicMethods
        public int Count()
        {
            return _stack.Count();
        }

        ///Modify this to later mark the elements on the _stack as delt or remove them from the stack.
        ///Probably remove them. Cards will live in the deck it was created in and the location it currently
        ///exists like a players hand, the stack, or discard.
        public IList<ICard> Deal(int count)
        {
            if (_stack.Count == 0)
            {
                throw new DeckOutofCards();
            }
            if (_stack.Count < count)
            {
                throw new DeckOutOfBounds();
            }

            var toDeal = _stack.Take(count).ToList();
            toDeal.ForEach(x => _stack.Remove(x));
            return toDeal;
        }

        //This method is a little odd. Not sure why I would want to order the cards, but PM knows best.
        public IList<ICard> OrderCards(bool ascending = true)
        {
            Shuffle();
            return _stack.OrderBy(x => x.CardSuit).ThenBy(x => x.Name).ToList();
        }

        //Randomly take all the cards from the decks in _deck and add references to _stack
        //Maybe use some threading someday to add more unpredictability...
        //Should I be able to shuffle if not all cards are in the hand?
        public void Shuffle()
        {
            _stack.Clear();
            foreach (var deck in _decks)
            {
                _stack.AddRange(deck.GetCards().OrderBy(x => StaticRandom.Rand()).ToList());
            }
        }
        #endregion PublicMethods
    }
}
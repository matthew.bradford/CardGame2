﻿using System;
using System.Collections.Generic;
using System.Linq;

using CardLib.Interface;

namespace CardLib.Object
{
    public class Hand : IHand
    {
        #region MemberVariables
        List<ICard> _hand = new List<ICard>();
        #endregion MemberVariables

        #region Constructors
        public Hand() { }
        #endregion Constructors

        #region Properties
        public int Count
        {
            get { return _hand.Count; }
        }
        #endregion Properties

        #region PublicMethods
        public IList<ICard> Get(CardName card)
        {
            var cards = _hand.Where(x => x.Name == card).ToList();
            cards.ForEach(x => _hand.Remove(x));
            return cards;
        }

        public void Add(IList<ICard> cards)
        {
            if (cards == null)
                return;
            
            _hand.AddRange(cards);
        }

        public void Add(ICard card)
        {
            if (card == null)
                return;

            _hand.Add(card);
        }

        public CardName RandomCardName()
        {
            int index = StaticRandom.Rand(_hand.Count-1);//0 base index
            return _hand[index].Name;
        }

        public IEnumerable<ICard> ShowHand()
        {
            return _hand.OrderBy(x => x.CardSuit).ThenBy(x => x.Name);
        }
        #endregion PublicMethods
    }
}
